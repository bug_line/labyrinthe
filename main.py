import pygame
import random


def min_distance(up_dist, down_dist, left_dist, right_dist):
    min_dist = 1000000

    if 0 < up_dist < min_dist:
        min_dist = up_dist

    if 0 < down_dist < min_dist:
        min_dist = down_dist

    if 0 < left_dist < min_dist:
        min_dist = left_dist

    if 0 < right_dist < min_dist:
        min_dist = right_dist

    return min_dist


class Grid:

    def wall_init(self):
        wall = []
        for i in range(self.size * 2 + 1):
            wall.append(-1)
        return wall

    def ligne_init(self):
        ligne = []
        for i in range(self.size):
            ligne.append(-1)
            ligne.append(random.randint(1000000, 8000000))
        ligne.append(-1)
        return ligne

    def break_side(self):
        self.grid[1][0] = self.grid[1][1]
        self.grid[self.finish_pos["ligne"]][self.finish_pos["collone"]]\
            = self.grid[self.finish_pos["ligne"]][self.finish_pos["collone"] - 1]

    def __init__(self, size, extra_break_wall, resolve):
        self.grid = []
        self.size = size
        self.start_pos = {"ligne": 1, "collone": 0}
        self.finish_pos = {"ligne": size * 2 - 1, "collone": size * 2}
        self.extra_break_wall = extra_break_wall
        self.resolve = resolve

        for x in range(size):
            self.grid.append(self.wall_init())
            self.grid.append(self.ligne_init())

        self.grid.append(self.wall_init())

    def draw(self, width, height):
        block_size = width / (self.size * 2 + 1)
        for x in range(self.size * 2 + 1):
            for y in range(self.size * 2 + 1):
                rect = pygame.Rect((width - block_size) * x / (self.size * 2),
                                   (height - block_size) * y / (self.size * 2), block_size, block_size)

                if self.grid[x][y] == -1:
                    pygame.draw.rect(screen, (0, 0, 0), rect)

                elif self.grid[x][y] == -2:
                    pygame.draw.rect(screen, (50, 200, 50), rect)

                else:
                    r = int(self.grid[x][y] / (255 * 255) + 150) % 255
                    g = int(self.grid[x][y] / 255) % 255
                    b = int(self.grid[x][y] + 150) % 255
                    pygame.draw.rect(screen, (r, g, b), rect)

    def random_wall(self):
        ligne, collone = 1, 1
        correct = False

        while self.grid[ligne][collone] != -1 or not correct:
            correct = False
            ligne = random.randint(1, self.size * 2 - 1)
            collone = random.randint(1, self.size * 2 - 1)

            if self.grid[ligne + 1][collone] != -1 and self.grid[ligne - 1][collone] != -1:
                correct = True

            if self.grid[ligne][collone + 1] != -1 and self.grid[ligne][collone - 1] != -1:
                correct = True

        return ligne, collone

    def wall_cassable(self):
        ligne, collone = 0, 0
        side_break = ""
        while side_break == "":
            ligne, collone = self.random_wall()

            if self.grid[ligne + 1][collone] != -1 and self.grid[ligne - 1][collone] != -1:
                if self.grid[ligne + 1][collone] != self.grid[ligne - 1][collone]:
                    if self.grid[ligne][collone + 1] == -1 and self.grid[ligne][collone - 1] == -1:
                        side_break = "up"

            if self.grid[ligne][collone + 1] != -1 and self.grid[ligne][collone - 1] != -1:
                if self.grid[ligne][collone + 1] != self.grid[ligne][collone - 1]:
                    if self.grid[ligne + 1][collone] == -1 and self.grid[ligne - 1][collone] == -1:
                        side_break = "side"

        return ligne, collone, side_break

    def block_around(self, pos):
        up = {"color": self.grid[pos["ligne"] - 1][pos["collone"]],
              "pos": {"ligne": pos["ligne"] - 1, "collone": pos["collone"]}}

        down = {"color": self.grid[pos["ligne"] + 1][pos["collone"]],
                "pos": {"ligne": pos["ligne"] + 1, "collone": pos["collone"]}}

        left = {"color": self.grid[pos["ligne"]][pos["collone"] - 1],
                "pos": {"ligne": pos["ligne"], "collone": pos["collone"] - 1}}

        right = {"color": self.grid[pos["ligne"]][pos["collone"] + 1],
                 "pos": {"ligne": pos["ligne"], "collone": pos["collone"] + 1}}

        return up, down, left, right

    def propagation(self, ligne, collone):
        color = self.grid[ligne][collone]
        pos2propage = [{"ligne": ligne, "collone": collone}]
        while len(pos2propage) != 0:
            up, down, left, right = self.block_around(pos2propage[0])

            if up["color"] != color and up["color"] != -1:
                self.grid[up["pos"]["ligne"]][up["pos"]["collone"]] = color
                pos2propage.append(up["pos"])

            if down["color"] != color and down["color"] != -1:
                self.grid[down["pos"]["ligne"]][down["pos"]["collone"]] = color
                pos2propage.append(down["pos"])

            if left["color"] != color and left["color"] != -1:
                self.grid[left["pos"]["ligne"]][left["pos"]["collone"]] = color
                pos2propage.append(left["pos"])

            if right["color"] != color and right["color"] != -1:
                self.grid[right["pos"]["ligne"]][right["pos"]["collone"]] = color
                pos2propage.append(right["pos"])

            del pos2propage[0]

    def break_wall(self):
        ligne, collone, side_break = self.wall_cassable()

        if side_break == "side":
            self.grid[ligne][collone] = self.grid[ligne][collone + 1]

        elif side_break == "up":
            self.grid[ligne][collone] = self.grid[ligne + 1][collone]

        self.propagation(ligne, collone)

    def breaks_random(self):
        for i in range(self.extra_break_wall):
            ligne, collone = self.random_wall()
            self.grid[ligne][collone] = self.grid[1][1]

    def distance2finish(self):
        self.grid[self.finish_pos["ligne"]][self.finish_pos["collone"]] = 1
        self.grid[self.finish_pos["ligne"]][self.finish_pos["collone"] - 1] = 2
        finish_block = {"ligne": 1, "collone": 0}
        block2look = [{"pos": {"ligne": self.finish_pos["ligne"], "collone": self.finish_pos["collone"] - 1},
                       "distance": 2}]

        while block2look[0]["pos"] != finish_block:
            up, down, left, right = self.block_around(block2look[0]["pos"])
            distance = block2look[0]["distance"] + 1

            if up["color"] >= 1000000:
                self.grid[up["pos"]["ligne"]][up["pos"]["collone"]] = distance
                block2look.append({"pos": up["pos"], "distance": distance})

            if down["color"] >= 1000000:
                self.grid[down["pos"]["ligne"]][down["pos"]["collone"]] = distance
                block2look.append({"pos": down["pos"], "distance": distance})

            if left["color"] >= 1000000:
                self.grid[left["pos"]["ligne"]][left["pos"]["collone"]] = distance
                block2look.append({"pos": left["pos"], "distance": distance})

            if right["color"] >= 1000000:
                self.grid[right["pos"]["ligne"]][right["pos"]["collone"]] = distance
                block2look.append({"pos": right["pos"], "distance": distance})

            del block2look[0]

    def solution(self):
        self.distance2finish()
        self.grid[1][0] = -2
        self.grid[1][1] = -2
        curr_pos = {"ligne": 1, "collone": 1}
        while curr_pos != self.finish_pos:
            up, down, left, right = self.block_around(curr_pos)
            up_dist, down_dist, left_dist, right_dist = up["color"], down["color"], left["color"], right["color"]
            min_dist = min_distance(up_dist, down_dist, left_dist, right_dist)

            if up_dist == min_dist:
                self.grid[up["pos"]["ligne"]][up["pos"]["collone"]] = -2
                curr_pos = up["pos"]

            elif down_dist == min_dist:
                self.grid[down["pos"]["ligne"]][down["pos"]["collone"]] = -2
                curr_pos = down["pos"]

            elif left_dist == min_dist:
                self.grid[left["pos"]["ligne"]][left["pos"]["collone"]] = -2
                curr_pos = left["pos"]

            elif right_dist == min_dist:
                self.grid[right["pos"]["ligne"]][right["pos"]["collone"]] = -2
                curr_pos = right["pos"]

            screen.fill((0, 0, 0))
            grid.draw(width, height)
            pygame.display.flip()

            pygame.time.wait(20)

    def verif_finish(self):
        finish = True
        color = self.grid[1][1]
        for x in range(len(self.grid)):
            for y in range(len(self.grid)):
                if self.grid[x][y] != color and self.grid[x][y] != -1:
                    finish = False

        if finish:
            self.break_side()
            self.breaks_random()
            if self.resolve:
                self.solution()

        return finish


pygame.init()
pygame.display.set_caption("labyrinthe")
res = width, height = 1000, 1000
screen = pygame.display.set_mode(res)
running = True
finish = False

size = 20
extra_break_wall = 10
resolve = True

grid = Grid(size, extra_break_wall, resolve)

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    if not finish:
        grid.break_wall()
        finish = grid.verif_finish()

    screen.fill((0, 0, 0))

    grid.draw(width, height)

    pygame.display.flip()
